#ifdef __MINGW32__
/* Required header file */
#include <fcntl.h>
#endif

#include <stdio.h>
#include <stdlib.h>


int main (int argc, char *argv[])
{
    int ch;
	int desloc;

	if (argc == 1) { desloc = 13;}
	else {
		desloc = atoi (argv[1]);
		if (desloc >= 0) { desloc = desloc % 26;}
		else { desloc = (26 - ((-desloc) % 26)) % 26; }
	}

#ifdef __MINGW32__
    /* Switch to binary mode */
    _setmode(_fileno(stdout),_O_BINARY);
    _setmode(_fileno(stdin),_O_BINARY);
#endif

    while ((ch = fgetc (stdin)) != EOF) {
        if (ch >= 'a' && ch <= 'z') {
            ch = (((ch - 'a') + desloc) % 26) + 'a';
        }
        else if (ch >= 'A' && ch <= 'Z') {
            ch = (((ch - 'A') + desloc) % 26) + 'A';
        }
        putc (ch, stdout);
    }

    return 0;
}
