/*

Implementacao base32 (RFC 4648 https://datatracker.ietf.org/doc/html/rfc4648 )
Usa entrada padrao e saida padrao apenas.
Funciona em modo binario mesmo no Windows (com Mingw32).
Ignora caracteres invalidos.
Para decodificar, opcao -d.
Ignora sinais = faltantes no final.
Suporta sinais = no meio do arquivo (concatenacao de dois arquivos b32).
Tamanho de linha maximo 72 na codificacao.

Para compilar:
gcc -Wall -Wextra -pedantic -O2 -s -static -o base32.exe base32.c

*/

#ifdef __MINGW32__
/* Required header file */
#include <fcntl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void decodificar (void)
{
    int ch;
    int i;
    int cont;
    unsigned char buf[5] = {0, 0, 0, 0, 0};
    int tab[256];
    int qtd_iguais = 0;

#ifdef __MINGW32__
    /* Switch to binary mode */
    _setmode(_fileno(stdout),_O_BINARY);
#endif

    i = 0;
    for (ch = 'A'; ch <= 'Z'; ch++) tab[ch] = i++;
    i = 0;
    for (ch = 'a'; ch <= 'z'; ch++) tab[ch] = i++;
    for (ch = '2'; ch <= '7'; ch++) tab[ch] = i++;

    cont = 0;
    qtd_iguais = 0;
    while ((ch = fgetc (stdin)) != EOF) {
        if ( ! (
                (ch >= 'A' && ch <= 'Z') ||
                (ch >= 'a' && ch <= 'z') ||
                (ch >= '2' && ch <= '7') ||
                ch == '='
            )
        ) {
            continue;
        }
        if (ch == '=') {
            qtd_iguais++;
            ch = 'A';
        }
        switch (cont) {
            case 0: /* ...00000 <-- mapeamento de qual byte cada bit pertence */
                buf[0] = tab[ch] << 3;
                break;
            case 1: /* ...00011 */
                buf[0] |= tab[ch] >> 2;
                buf[1] = (tab[ch] & 0x3) << 6;
                break;
            case 2: /* ...11111 */
                buf[1] |= tab[ch] << 1;
                break;
            case 3: /* ...12222 */
                buf[1] |= tab[ch] >> 4;
                buf[2] = (tab[ch] & 0xf) << 4;
                break;
            case 4: /* ...22223 */
                buf[2] |= tab[ch] >> 1;
                buf[3] = (tab[ch] & 0x1) << 7;
                break;
            case 5: /* ...33333 */
                buf[3] |= tab[ch] << 2;
                break;
            case 6: /* ...33444 */
                buf[3] |= tab[ch] >> 3;
                buf[4] = (tab[ch] & 0x7) << 5;
                break;
            case 7: /* ...44444 */
                buf[4] |= tab[ch];
                if (qtd_iguais < 7) fputc (buf[0], stdout);
                if (qtd_iguais < 5) fputc (buf[1], stdout);
                if (qtd_iguais < 4) fputc (buf[2], stdout);
                if (qtd_iguais < 2) fputc (buf[3], stdout);
                if (qtd_iguais < 1) fputc (buf[4], stdout);
                qtd_iguais = 0;
                break;
            default: break;
        }
        cont = ((cont + 1) & 0x7);
    }
    /* gravacao dos bytes finais caso tenham sido omitidos os sinais '=' */
    if (cont > 1) fputc (buf[0], stdout);
    if (cont > 3) fputc (buf[1], stdout);
    if (cont > 4) fputc (buf[2], stdout);
    if (cont > 6) fputc (buf[3], stdout);
}

void trata_quebra_linha(int *cont_linha)
{
    (*cont_linha)++;
    if (*cont_linha >= 72) {
        *cont_linha = 0;
        printf ("\n");
    }
}

void codificar (void)
{
    int i;
    int ch, ch_ant;
    int cont, cont_linha;
    char tab[33] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

#ifdef __MINGW32__
    /* Switch to binary mode */
    _setmode(_fileno(stdin),_O_BINARY);
#endif

    cont = 0;
    cont_linha = 0;
    ch_ant = 0;
    while ((ch = fgetc (stdin)) != EOF) {
        switch (cont) {
            case 0:
                putc (tab[ch >> 3], stdout);
                break;
            case 1:
                putc (tab[((ch_ant & 0x7) << 2) | (ch >> 6)], stdout);
                putc (tab[((ch >> 1) & 0x1f)], stdout);
                trata_quebra_linha(&cont_linha);
                break;
            case 2:
                putc (tab[((ch_ant & 0x1) << 4) | (ch >> 4)], stdout);
                break;
            case 3:
                putc (tab[((ch_ant & 0xf) << 1) | (ch >> 7)], stdout);
                putc (tab[((ch >> 2) & 0x1f)], stdout);
                trata_quebra_linha(&cont_linha);
                break;
            case 4:
                putc (tab[((ch_ant & 0x3) << 3) | (ch >> 5)], stdout);
                putc (tab[ch & 0x1f], stdout);
                trata_quebra_linha(&cont_linha);
                break;
            default: break;
        }
        cont++;
        if (cont == 5) cont = 0;
        trata_quebra_linha(&cont_linha);
        ch_ant = ch;
    }

    if (cont == 1) {
        putc (tab[(ch_ant & 0x7) << 2], stdout);
        trata_quebra_linha(&cont_linha);
        for (i = 0; i < 6; i++) {
            putc ('=', stdout);
            trata_quebra_linha(&cont_linha);
        }
    }
    else if (cont == 2) {
        putc (tab[(ch_ant & 0x1) << 4], stdout);
        trata_quebra_linha(&cont_linha);
        for (i = 0; i < 4; i++) {
            putc ('=', stdout);
            trata_quebra_linha(&cont_linha);
        }
    }
    else if (cont == 3) {
        putc (tab[(ch_ant & 0xf) << 1], stdout);
        trata_quebra_linha(&cont_linha);
        for (i = 0; i < 3; i++) {
            putc ('=', stdout);
            trata_quebra_linha(&cont_linha);
        }
    }
    else if (cont == 4) {
        putc (tab[(ch_ant & 0x3) << 3], stdout);
        trata_quebra_linha(&cont_linha);
        putc ('=', stdout);
        trata_quebra_linha(&cont_linha);
    }
    if (cont_linha != 0) printf ("\n");
}

int main (int argc, char *argv[])
{
    int i;

    for (i = 1; i < argc; i++) {
        if (strcmp (argv[i], "-d") == 0) {
            decodificar ();
            break;
        }
    }

    if (i >= argc) codificar ();

    return 0;
}
