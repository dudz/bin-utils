/* UTF-8

Traduz binários para representação Unicode, usando as seguintes conversões:
00: Representação do símbolo "NUL"
01-1F: Unicode com desenho da tabela DOS 437
20-7E: ASCII
7F: branco
80-9F: Unicode com desenho da tabela Windows-1252 (brancos nos 5 que não se aplicarem)
A0-FF: Unicode com desenho da tabela ISO-8859-1

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



void CarregaTabelaConversao (char tab_conv[256][4]);

int main (int argc, char *argv[])
{
    char tab_conv[256][4];
    long tam_linha, cont;
    int caracter;
    FILE *fi;

    if (argc != 3 && (argc != 4 || strcmp (argv[1], "-no-w") != 0)) {
        fprintf (stderr,
            "Sintaxe: bin2txt tamanho_linha arquivo_entrada\n"
            "ou       bin2txt -no-w tamanho_linha arquivo_entrada\n"
            "Onde:\n"
            "tamanho_linha: quantidade maxima de caracteres em cada linha.\n"
            "               Entre 0 para nao inserir quebras de linha.\n"
            "arquivo_entrada: arquivo binario a converter para texto Unicode.\n"
            "-no-w: desabilita aviso para quando forem desabilitadas as quebras de linha.\n"
        );
        (void) getchar ();
        exit (1);
    }

    if (argc == 3) tam_linha = atol (argv[1]); else tam_linha = atol (argv[2]);
    if (tam_linha < 1 && (argc != 4 || strcmp (argv[1], "-no-w") != 0)) {
        fprintf (stderr, "Sem as quebras de linha sera gerado um arquivo texto com apenas uma\n"
                         "linha muito extensa, ideal apenas para buscas automaticas com find\n"
                         "ou grep. Deseja continuar? (S/N) "
        );
        fflush (stderr);
        caracter = getchar ();
        (void) getchar ();
        if (caracter != 's' && caracter != 'S') exit (0);
    }

    CarregaTabelaConversao (tab_conv);

    fi = fopen (argv[argc - 1], "rb");
    if (fi == NULL) {
        fprintf (stderr, "Erro ao abrir %s\n", argv[argc - 1]);
        (void) getchar ();
        exit (1);
    }
    cont = 0;
    while ((caracter = fgetc (fi)) != EOF) {
        if (cont >= tam_linha && tam_linha > 0) {
            cont = 0;
            printf ("\n");
        }
        caracter = (caracter + 256) & 0xff;
        printf ("%s", tab_conv[caracter]);
        cont++;
    }
    fclose (fi);
    return 0;
}

void CarregaTabelaConversao (char tab_conv[256][4])
{
    int i;
    long tab[256];

    tab[0x00] = ' ';    /* Segundo wikepedia, o certo seria
                           0x2007, que é espaço tabular, mas o
                           notepad não exibe. O caracter U+2400 é
                           representação do símbolo NUL, mas deixa
                           muito poluído, e não funciona em fonte
                           Courier New.
                        */
    tab[0x01] = 0x263A;
    tab[0x02] = 0x263B;
    tab[0x03] = 0x2665;
    tab[0x04] = 0x2666;
    tab[0x05] = 0x2663;
    tab[0x06] = 0x2660;
    tab[0x07] = 0x2022;
    tab[0x08] = 0x25D8;
    tab[0x09] = 0x25CB;
    tab[0x0A] = 0x25D9;
    tab[0x0B] = 0x2642;
    tab[0x0C] = 0x2640;
    tab[0x0D] = 0x266A;
    tab[0x0E] = 0x266B;
    tab[0x0F] = 0x263C;
    tab[0x10] = 0x25BA;
    tab[0x11] = 0x25C4;
    tab[0x12] = 0x2195;
    tab[0x13] = 0x203C;
    tab[0x14] = 0x00B6;
    tab[0x15] = 0x00A7;
    tab[0x16] = 0x25AC;
    tab[0x17] = 0x21A8;
    tab[0x18] = 0x2191;
    tab[0x19] = 0x2193;
    tab[0x1A] = 0x2192;
    tab[0x1B] = 0x2190;
    tab[0x1C] = 0x221F;
    tab[0x1D] = 0x2194;
    tab[0x1E] = 0x25B2;
    tab[0x1F] = 0x25BC;
    for (i = 0x20; i <= 0x7E; i++) tab[i] = i;
    tab[0x7F] = ' ';
    tab[0x80] = 0x20AC;
    tab[0x81] = ' ';
    tab[0x82] = 0x201A;
    tab[0x83] = 0x0192;
    tab[0x84] = 0x201E;
    tab[0x85] = 0x2026;
    tab[0x86] = 0x2020;
    tab[0x87] = 0x2021;
    tab[0x88] = 0x02C6;
    tab[0x89] = 0x2030;
    tab[0x8A] = 0x0160;
    tab[0x8B] = 0x2039;
    tab[0x8C] = 0x0152;
    tab[0x8D] = ' ';
    tab[0x8E] = 0x017D;
    tab[0x8F] = ' ';
    tab[0x90] = ' ';
    tab[0x91] = 0x2018;
    tab[0x92] = 0x2019;
    tab[0x93] = 0x201C;
    tab[0x94] = 0x201D;
    tab[0x95] = 0x2022;
    tab[0x96] = 0x2013;
    tab[0x97] = 0x2014;
    tab[0x98] = 0x02DC;
    tab[0x99] = 0x2122;
    tab[0x9A] = 0x0161;
    tab[0x9B] = 0x203A;
    tab[0x9C] = 0x0153;
    tab[0x9D] = ' ';
    tab[0x9E] = 0x017E;
    tab[0x9F] = 0x0178;
    for (i = 0xA0; i <= 0xFF; i++) tab[i] = i;

    for (i = 0; i < 256; i++) {
        if (tab[i] < 128) {
            tab_conv[i][0] = tab[i];
            tab_conv[i][1] = '\0';
        }
        else if (tab[i] < 0x800) {
            /* 110xxxyy 10yyyyyy */
            tab_conv[i][0] = (3 << 6) | (tab[i] >> 6);
            tab_conv[i][1] = (1 << 7) | (tab[i] & 0x3f);
            tab_conv[i][2] = '\0';
        }
        else {
            /* 1110xxxx 10xxxxyy 10yyyyyy */
            tab_conv[i][0] = (7 << 5) | (tab[i] >> 12);
            tab_conv[i][1] = (1 << 7) | ((tab[i] >> 6) & 0x3f);
            tab_conv[i][2] = (1 << 7) | (tab[i] & 0x3f);
            tab_conv[i][3] = '\0';
        }
    }
}
