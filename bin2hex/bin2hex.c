/*

Programa para converter arquivo binario para base 16 (hexadecimal).

gcc -Wall -Wextra -pedantic -s -O2 -o bin2hex.exe bin2hex.c

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __MINGW32__
/* Required header file */
#include <fcntl.h>
#endif
int main (int argc, char *argv[])
{
	int ch;
    int cont = 0;
	
	if (argc > 2 ||
		(
			argc == 2 &&
			(
				strcmp(argv[1], "--help") == 0 ||
				strcmp(argv[1], "-h") == 0 ||
				strcmp(argv[1], "/h") == 0 ||
				strcmp(argv[1], "/?") == 0 ||
				strcmp(argv[1], "-?") == 0
			)
		)
	) {
			printf (
				"Programa que converte arquivo binario para codigos hexadecimais.\n"
				"Utiliza entrada e saida padroes\n"
				"Exemplo: bin2hex < entrada.bin > saida.txt\n"
			);
			exit (0);
	}
	
#ifdef __MINGW32__
/* Switch to binary mode */
_setmode(_fileno(stdin),_O_BINARY);
/* _setmode(_fileno(stdout),_O_BINARY); */
#endif
	while ((ch = getchar()) != EOF) {
        printf ("%02X", ch);
        cont += 2;
        if (cont >= 72) {
            cont = 0;
            printf ("\n");
        }
	}
	if (cont > 0) printf("\n");
	return 0;
}
