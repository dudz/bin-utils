#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __MINGW32__
/* Required header file */
#include <fcntl.h>
#endif
int main (int argc, char *argv[])
{
	int ch;
	int q;
	int p;
	
	if (argc > 2 ||
		(
			argc == 2 &&
			(
				strcmp(argv[1], "--help") == 0 ||
				strcmp(argv[1], "-h") == 0 ||
				strcmp(argv[1], "/h") == 0 ||
				strcmp(argv[1], "/?") == 0 ||
				strcmp(argv[1], "-?") == 0
			)
		)
	) {
			printf (
				"Programa que converte codigos hexadecimais para arquivo binario.\n"
				"Utiliza entrada e saida padroes\n"
				"Caracteres invalidos sao ignorados\n"
				"Exemplo: hex2bin < entrada.txt > saida.txt\n"
			);
			exit (0);
	}
	
#ifdef __MINGW32__
/* Switch to binary mode */
_setmode(_fileno(stdout),_O_BINARY);
#endif
	q = 0;
	p = 0;
	while ((ch = getchar()) != EOF) {
		if (ch >= '0' && ch <= '9') ch -= '0';
		else if (ch >= 'a' && ch <= 'z') ch -= 'a' - 10;
		else if (ch >= 'A' && ch <= 'Z') ch -= 'A' - 10;
		else ch = -1;
		if (ch < 0) continue;
		q++;
		p = p * 16 + ch;
		if (q == 2) {
			q = 0;
			putchar (p);
			p = 0;
		}
	}
	if (q == 1) putchar (p * 16);
	return 0;
}
