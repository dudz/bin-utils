#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int nExtras (int n, int *pa, int *pl)
{
	int l, a;
	int nPixels;
	int nE = 0x7fffffff, nEAtu;
	int lMax, lMin;
	double lMaxDouble;

	*pa = *pl = -1;
	nPixels = (n + 1 + ((3 - ((n + 1) % 3)) % 3)) / 3;
	lMin = (int) sqrt(nPixels);
	lMaxDouble = 4.0 * sqrt(nPixels / 12.0);
	lMax = (int) lMaxDouble;
	if (lMax < lMaxDouble) lMax++;
	for (l = lMax; l >= lMin; l--) {
		a = (nPixels / l) + (nPixels % l == 0 ? 0 : 1);
		nEAtu = a * l * 3 - n;
		if (nEAtu < nE) {
			nE = nEAtu;
			*pa = a;
			*pl = l;
		}
	}
	return (nE);
}

int main (int argc, char *argv[])
{
	int tam_orig;
	int L, a;
	int i;
	if (argc != 3 ||
		(argv[1][0] != 'i' && argv[1][0] != 'I' &&
			argv[1][0] != 'f' && argv[1][0] != 'f'
		) 
		||
		(tam_orig = atoi(argv[2])) < 1
	) {
		fprintf(stderr, "Sintaxe %s <ini|fim> tamanho\n", argv[0]);
		exit (1);
	}
	(void) nExtras (tam_orig, &a, &L);
	if (argv[1][0] == 'i' || argv[1][0] == 'I') {
		printf ("P6 %d %d 255 ", L, a);
	}
	else {
		printf ("!");
		for (i = L * a * 3 - tam_orig - 1; i > 0; i--) printf("x");
	}
	fflush (stdout);
	return 0;
}

