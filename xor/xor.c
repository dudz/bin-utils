/*
gcc -Wall -Wextra -pedantic -ansi -O3 -o xor xor.c

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM_BUF 16384

void Le_f2 (unsigned char *buf2, int n_bytes, FILE *f2, unsigned long tam2, unsigned long *pos_f2);


void Usage (char *s)
{
	fprintf (stderr,
		"Programa que efetua XOR entre dois arquivos binarios.\n"
		"Uso: %s arq_entrada_1 arq_entrada_2 arq_saida [-r|-t]\n"
		"    -r Repetir o arquivo menor ate' completar o tamanho do maior.\n"
		"    -t Truncar o arquivo maior para o tamanho do menor na geracao da saida.\n"
		, s
	);
	exit (1);
}


int main (int argc, char *argv[])
{
	FILE *f1, *f2, *fo;
	unsigned long tam1, tam2;
	unsigned long pos_f2 = 0;
	unsigned char *buf1, *buf2;
	int i, qtd;

	if (TAM_BUF < 16 || TAM_BUF % 4 != 0 || sizeof (int) != 4) {
		fprintf (stderr, "Erro: programa deve ser compilado com 32 bits.\n");
		exit (1);
	}

	if (argc != 4 && argc != 5) {
		Usage (argv[0]);
	}

	f1 = fopen (argv[1], "rb");
	if (f1 == NULL) {
		fprintf (stderr, "Erro ao abrir %s\n", argv[1]);
		perror("Erro: ");
		exit (1);
	}
	else {
		fseek (f1, 0, SEEK_END);
		tam1 = ftell (f1);
		fseek (f1, 0, SEEK_SET);
		if (tam1 == 0) {
			fclose (f1);
			fprintf (stderr, "Arquivo %s tem tamanho zerado.\n", argv[1]);
			exit (1);
		}
	}

	f2 = fopen (argv[2], "rb");
	if (f2 == NULL) {
		fclose (f1);
		fprintf (stderr, "Erro ao abrir %s\n", argv[2]);
		exit (1);
	}
	else {
		fseek (f2, 0, SEEK_END);
		tam2 = ftell (f2);
		fseek (f2, 0, SEEK_SET);
		if (tam2 == 0) {
			fclose (f1);
			fclose (f2);
			fprintf (stderr, "Arquivo %s tem tamanho zerado.\n", argv[2]);
			exit (1);
		}
	}

	if (tam1 != tam2) {
		if (
			argc != 5 || (
				strcmp (argv[4], "-r") != 0 &&
				strcmp (argv[4], "-t") != 0
			)
		)
		{
			fprintf (stderr, "Arquivos tem tamanhos diferentes. Obrigatorio especificar -r ou -t.\n");
			exit (1);
		}
		else {
			if (argv[4][1] == 't') {
				if (tam1 < tam2) tam2 = tam1; else tam1 = tam2;
			}
		}
	}

	if (tam1 < tam2) {
		unsigned long tmp = tam1;
		tam1 = tam2;
		tam2 = tmp;
		fo = f1;
		f1 = f2;
		f2 = fo;
		fo = NULL;
	}

	fo = fopen (argv[3], "wb");
	if (fo == NULL) {
		fclose (f1);
		fclose (f2);
		fprintf (stderr, "Erro ao criar %s.\n", argv[3]);
		exit (1);
	}

	buf1 = (unsigned char *) malloc (TAM_BUF);
	buf2 = (unsigned char *) malloc (TAM_BUF);
	if (buf1 == NULL || buf2 == NULL) {
		fprintf (stderr, "Sem memoria\n");
		exit (1);
	}

	pos_f2 = 0;
	qtd = (int)(tam1 / TAM_BUF);
	for (; qtd > 0; qtd--) {
		if (fread (buf1, TAM_BUF, 1, f1) != 1) {
			fprintf (stderr, "Erro ao ler f1 (1)\n");
			exit (1);
		}
		Le_f2 (buf2, TAM_BUF, f2, tam2, &pos_f2);
		/* for (i = 0; i < TAM_BUF; i++) {buf1[i] ^= buf2[i];} */
		for (i = (TAM_BUF >> 2) - 1; i >= 0; i--) {
			/*buf1[i] ^= buf2[i];*/
			((int *)buf1)[i] ^= ((int *)buf2)[i];
		}
		fwrite (buf1, TAM_BUF, 1, fo);
	}
	qtd = (int)(tam1 % TAM_BUF);
	if (qtd > 0) {
		if ((int) fread (buf1, qtd, 1, f1) != 1) {
			fprintf (stderr, "Erro ao ler f1 (2)\n");
			exit (1);
		}
		Le_f2 (buf2, qtd, f2, tam2, &pos_f2);
		for (i = (qtd / 4) - 1; i >= 0; i--) {
			/*buf1[i] ^= buf2[i];*/
			((int *)buf1)[i] ^= ((int *)buf2)[i];
		}
		for (i = (qtd & 0x3); i > 0; i--) {
			buf1[(int)(tam1 % TAM_BUF) - i] ^= buf2[(int)(tam1 % TAM_BUF) - i];
		}
		fwrite (buf1, qtd, 1, fo);
	}

	fclose (fo);
	fclose (f2);
	fclose (f1);
	free (buf1);
	free (buf2);
	return 0;
}

/* funcao para leitura do arquivo menor. Deve ser usada com um único arquivo */
void Le_f2 (unsigned char *buf2, int n_bytes, FILE *f2, unsigned long tam2, unsigned long *pos_f2)
{
	int i;
	static int iniciado = 0;
	static int pt = 0;
	static unsigned char buf3[TAM_BUF];

	/* se arquivo f2 for muito pequeno, preencher com apenas uma leitura, para
	 * evitar excesso de leituras.
	 */
	if (tam2 <= TAM_BUF) {
		if (iniciado == 0) {
			iniciado = 1;
			if (fread (buf3, tam2, 1, f2) != 1) {
				fprintf (stderr, "Erro ao ler f2 (0)\n");
				exit (1);
			}
			pt = 0;
			*pos_f2 = 0;
			fseek (f2, *pos_f2, SEEK_SET);
		}
		i = 0;
		while (i < n_bytes) {
			buf2[i] = buf3[pt];
			pt++;
			if (pt == (int) tam2) pt = 0;
			i++;
		}
		return;
	}

	/*
		*pos_f2: quantos já foram lidos na posição do arquivo
		i: quantos já leu no preenchimento atual do buffer
		n_bytes: quanto quer ler no total
		(n_bytes - i): quanto ainda falta ler
	*/
	i = 0;
	while (i < n_bytes) {
		if (*pos_f2 + (n_bytes - i) <= tam2) {
			if ((int) fread (buf2 + i, n_bytes - i, 1, f2) != 1) {
				fprintf (stderr, "Erro ao ler f2 (1)\n");
				exit (1);
			}
			*pos_f2 += (n_bytes - i);
			i = n_bytes;
			if (*pos_f2 == tam2) {
				*pos_f2 = 0;
				fseek (f2, 0, SEEK_SET);
			}
		}
		else {
			if (fread (buf2 + i, tam2 - *pos_f2, 1, f2) != 1) {
				fprintf (stderr, "Erro ao ler f2 (2)\n");
				exit (1);
			}
			i += (tam2 - *pos_f2);
			*pos_f2 = 0;
			fseek (f2, 0, SEEK_SET);
		}
	}
}
