/*
gcc -Wall -Wextra -pedantic -O2 -c -o codimg2.o codimg2.c

C:\Users\27780785840\Documents\geral\c\codimg>

Nova versão do codimg, utilizando algoritmo de Bresenham para distribuição
dos bits na camada mais significativa a ser alterada.

Alterado para tratar apenas arquivos PPM binários (com 24 bits de cores),
assim evita-se a perda que ocorria nos BMP cujas linhas não fossem
múltiplas de 4, além de simplificar. Para fazer com 48 bits de cores
será necessário antes ocupar os bits menos significativos do pixel, e não
distribuir byte a byte, então será implementado futuramente.

Como o conceito de bloco não tem mais importância, a distribuição se dá de forma
mais direta byte a byte, e não mais por níveis dentro de blocos, como era antes (era
do menos para o mais significativo).

Como a distribuição de Bresenham é perfeita, não é mais necessário o tratamento
da quantidade de blocos, e os mesmos são usados apenas para deixar mais rápido.
*/

#include <stdio.h>
#include <stdlib.h>

typedef unsigned long ulong;
typedef unsigned char byte;

ulong flen (FILE *fd);
void Codifica (char *arqTxt, char *arqImg, char *arqOut);
void DeCodifica (char *arqTxt, char *arqImg);
int ExtraiCabecalhoImagem (FILE *fimg, ulong *tam_dados);
int ExtraiCabecalhoImagemPPM (FILE *fimg, ulong *tam_dados);
ulong RotEsq (ulong n);
void Fputc (int ch, FILE *fo);

int main (int argc, char *argv[])
{
	if (argc != 4 && argc != 3) {
		fprintf (stderr, "sintaxe:\n"
			 "\tcodificar: %s secreto.txt img.ppm out.ppm\n"
			 "\tdecodificar: %s img.ppm secreto.txt\n",
			 argv[0], argv[0]
		);
		return 1;
	}
	if (argc == 4) {
		Codifica (argv[1], argv[2], argv[3]);
	}
	else if (argc == 3) {
		DeCodifica (argv[2], argv[1]);
	}
	return 0;
}


void Codifica (char *arqTxt, char *arqImg, char *arqOut)
{
/*
Algoritmo de Bresenham:
plotLine(x0,y0, x1,y1)
  dx = x1 - x0
  dy = y1 - y0
  D = 2*dy - dx
  y = y0

  for x from x0 to x1
	plot(x,y)
	if D > 0
	   y = y + 1
	   D = D - 2 * dx
	end if
	D = D + 2 * dy
*/

	FILE *ftxt, *fimg, *fout;

	char buf[4096];
	int qtd_buf, pt_buf;
	int bits_txt, ch_txt;
	int qtd_bits_txt;
	int dx, dy;
	int dx2, dy2;
	int qtd_camadas_cheias;
	int qtd_mascarar;
	int u; /* grava bit extra na ultima camada? */
	int x;
	int D;
	int i;
	ulong tam_txt, tam_dados;

	
	ftxt = fopen (arqTxt, "rb");
	if (ftxt == NULL) {
		fprintf (stderr, "Erro ao abrir %s\n", arqTxt);
		exit (1);
	}
	fimg = fopen (arqImg, "rb");
	if (fimg == NULL) {
		fclose (ftxt);
		fprintf (stderr, "Erro ao abrir %s\n", arqImg);
		exit (1);
	}
	tam_txt = flen (ftxt);
	if (tam_txt == 0) {
		fprintf (stderr, "Nao ha o que codificar\n");
		fclose (ftxt);
		fclose (fimg);
		exit (1);
	}

	if ((i = ExtraiCabecalhoImagem (fimg, &tam_dados)) != 0) {
		fprintf (stderr, "Erro (%d) no cabecalho da imagem\n", i);
		fclose (ftxt);
		fclose (fimg);
		exit (1);
	}

	if (tam_txt > tam_dados - 32) {
		fprintf (stderr, "Imagem muito pequena para guardar codificacao\n");
		fclose (ftxt);
		fclose (fimg);
		exit (1);
	}
	
	fout = fopen (arqOut, "wb");
	if (fout == NULL) {
		fclose (ftxt);
		fclose (fimg);
		fprintf (stderr, "Erro ao criar %s\n", arqOut);
		exit (1);
	}
	
	/* cabeçalho da saída, idêntico ao da entrada */
	i = ftell (fimg);
	fseek (fimg, 0, SEEK_SET);
	while (i > 0) {
		Fputc (fgetc (fimg), fout);
		i--;
	}

	/* codificacao do tamanho do txt */
	for (i = 0; i < 32; i++) {
		tam_txt = RotEsq (tam_txt);
		Fputc ((tam_txt & 1) | (fgetc (fimg) & 0xfe), fout);
	}

	dx = tam_dados - 32; /* quantidade de bytes em que pode distribuir */
	qtd_camadas_cheias = (tam_txt * 8) / dx;
	dy = (tam_txt * 8) - (qtd_camadas_cheias * dx); /* bits na camada mais significativa, a distribuir com Bresenham */
	if (dy == 0) {
		qtd_camadas_cheias--;
		dy = (tam_txt * 8) - (qtd_camadas_cheias * dx);
	}
	if (qtd_camadas_cheias > 7) {
		fprintf (stderr, "Erro: qtd_camadas_cheias maior que 7.\n");
		exit (1);
	}
	if (dy < 1) {
		fprintf (stderr, "Erro: sem bits a distribuir na ultima camada.\n");
		exit (1);
	}
	D = 2 * dy - dx;
	qtd_buf = 0;
	pt_buf = 4096;
	qtd_bits_txt = 0;
	ch_txt = 0;
	bits_txt = 0;
    dx2 = 2 * dx;
    dy2 = 2 * dy;
	for (x = 0; x < dx; x++) { /* loop para cada byte da area de dados após os 32 iniciais */
		if (D > 0) {
			u = 1;
			D -= dx2;
		}
		else u = 0;
		D += dy2;
		if (qtd_buf < 1) {
			pt_buf = 0;
			qtd_buf = 4096;
			if (qtd_buf > dx - x) qtd_buf = dx - x;
			if (fread (buf, qtd_buf, 1, fimg) != 1) {
				fprintf (stderr, "Erro na leitura, nao leu bloco completo de fimg.\n");
				exit (1);
			}
		}
		qtd_mascarar = qtd_camadas_cheias + u;
		if (qtd_bits_txt < qtd_mascarar) {
			qtd_bits_txt += 8;
			if (qtd_bits_txt < qtd_mascarar) {
				fprintf (stderr, "Erro: qtd_bits_txt ainda menor que qtd_mascarar.\n");
				exit (1);
			}
			ch_txt = fgetc (ftxt);
			if (ch_txt == EOF) {
				fprintf (stderr, "Fim inesperado de ftxt.\n");
				exit (1);
			}
			bits_txt = (bits_txt << 8) | (ch_txt & 0xff);
		}
		buf[pt_buf] &= (0xff << qtd_mascarar);
		buf[pt_buf] |= (bits_txt >> (qtd_bits_txt - qtd_mascarar));
		bits_txt &= (~(0xffff << (qtd_bits_txt - qtd_mascarar)));
		qtd_bits_txt -= qtd_mascarar;
		Fputc (buf[pt_buf], fout);
		pt_buf++;
		qtd_buf--;
	}
	if (qtd_bits_txt != 0) {
		fprintf (stderr, "Erro: sobraram %d bits em bits_txt.\n", qtd_bits_txt);
		exit (1);
	}
	if (fgetc (ftxt) != EOF) {
		fprintf (stderr, "Erro: nao terminou arquivo ftxt.\n");
		exit (1);
	}
	if (qtd_buf != 0) {
		fprintf (stderr, "Erro: sobraram %d bytes em buf.\n", qtd_buf);
		exit (1);
	}
	if (fgetc (fimg) != EOF) {
		fprintf (stderr, "Erro: nao terminou arquivo fimg.\n");
		exit (1);
	}

	Fputc (0, (FILE *) NULL); /* flush da saída */
	fclose (ftxt);
	fclose (fimg);
	fclose (fout);
	printf ("Foram alterados no maximo %d bits menos significativos de cada byte.\n", 1 + qtd_camadas_cheias);
}

void DeCodifica (char *arqTxt, char *arqImg)
{
	int i;
    ulong tam_txt;
    ulong tam_dados;
    FILE *ftxt, *fimg;
	char buf[4096];
	int qtd_buf, pt_buf;
	int u; /* recupera bit extra na ultima camada? */
	int dx, dy;
	int dx2, dy2;
	int qtd_camadas_cheias;
	int qtd_mascarados;
	int x;
	int D;
	int bits_txt;
	int qtd_bits_txt;
	
	fimg = fopen (arqImg, "rb");
	if (fimg == NULL) {
		fprintf (stderr, "Erro ao abrir %s\n", arqImg);
		exit (1);
	}
	
	if ((i = ExtraiCabecalhoImagem (fimg, &tam_dados)) != 0) {
		fprintf (stderr, "Erro (%d) no cabecalho da imagem\n", i);
		fclose (fimg);
		exit (1);
	}

    if (tam_dados < 33) {
        fclose (fimg);
        fprintf (stderr, "Imagem e' pequena, nao contem conteudo secreto\n");
        exit (1);
    }
	
    tam_txt = 0;
    for (i = 0; i < 32; i++) tam_txt = (tam_txt << 1) | (fgetc (fimg) & 1);
    if (tam_txt > tam_dados - 32 || tam_txt < 1) {
        fclose (fimg);
        fprintf (stderr, "Imagem nao contem conteudo secreto\n");
        exit (1);
    }

    ftxt = fopen (arqTxt, "wb");
    if (ftxt == NULL) {
        fclose (fimg);
        fprintf (stderr, "Erro ao criar %s\n", arqTxt);
        exit (1);
    }

	dx = tam_dados - 32; /* quantidade de bytes em que foi feita distribuição dos bits */
	qtd_camadas_cheias = (tam_txt * 8) / dx;
	dy = (tam_txt * 8) - (qtd_camadas_cheias * dx); /* bits na camada mais significativa, distribuídos com Bresenham */
	if (dy == 0) {
		qtd_camadas_cheias--;
		dy = (tam_txt * 8) - (qtd_camadas_cheias * dx);
	}
	if (qtd_camadas_cheias > 7) {
		fprintf (stderr, "Erro: qtd_camadas_cheias maior que 7.\n");
		exit (1);
	}
	if (dy < 1) {
		fprintf (stderr, "Erro: sem bits distribuidos na ultima camada.\n");
		exit (1);
	}
	D = 2 * dy - dx;
	qtd_buf = 0;
	pt_buf = 4096;
	qtd_bits_txt = 0;
	bits_txt = 0;
    dx2 = 2 * dx;
    dy2 = 2 * dy;
	for (x = 0; x < dx; x++) { /* loop para cada byte da area de dados após os 32 iniciais */
		if (D > 0) {
			u = 1;
			D -= dx2;
		}
		else u = 0;
		D += dy2;
		if (qtd_buf < 1) {
			pt_buf = 0;
			qtd_buf = 4096;
			if (qtd_buf > dx - x) qtd_buf = dx - x;
			if (fread (buf, qtd_buf, 1, fimg) != 1) {
				fprintf (stderr, "Erro na leitura, nao leu bloco completo de fimg.\n");
				exit (1);
			}
		}
		qtd_mascarados = qtd_camadas_cheias + u;
		bits_txt <<= qtd_mascarados;
		bits_txt |= (((int)buf[pt_buf]) & ((~(0xff << qtd_mascarados)) & 0xff));
		qtd_bits_txt += qtd_mascarados;
		if (qtd_bits_txt >= 8) {
			Fputc ((bits_txt >> (qtd_bits_txt - 8)) & 0xff, ftxt);
			qtd_bits_txt -= 8;
			bits_txt &= ((~(0xff << qtd_bits_txt)) & 0xff);
		}
		pt_buf++;
		qtd_buf--;
	}
	if (qtd_bits_txt != 0) {
		fprintf (stderr, "Erro: sobraram %d bits em bits_txt.\n", qtd_bits_txt);
		exit (1);
	}
	if (qtd_buf != 0) {
		fprintf (stderr, "Erro: sobraram %d bytes em buf.\n", qtd_buf);
		exit (1);
	}
	if (fgetc (fimg) != EOF) {
		fprintf (stderr, "Erro: nao terminou arquivo fimg.\n");
		exit (1);
	}
	Fputc (0, (FILE *) NULL); /* flush da saída */
    fclose (ftxt);
    fclose (fimg);
	printf ("Estavam alterados no maximo %d bits menos significativos de cada byte.\n", 1 + qtd_camadas_cheias);
}


/* Pode aceitar novos formatos no futuro */
int ExtraiCabecalhoImagem (FILE *fimg, ulong *tam_dados)
{
	return ExtraiCabecalhoImagemPPM (fimg, tam_dados);
}

/* retorna diferente de zero se houver erro */
int ExtraiCabecalhoImagemPPM (FILE *fimg, ulong *tam_dados)
{
	int dim[3];
	int i;
	int ch;
  
	/* assinatura ppm binário */
	if (fgetc (fimg) != 'P' || fgetc (fimg) != '6') return 1;
	(void) fgetc (fimg);
	dim[0] = dim[1] = dim[2] = 0;
	for (i = 0; i < 3; i++) {
		for (;;) {
			ch = fgetc (fimg);
			if (ch == EOF) return 2;
			if (ch == '#') {
				do ch = fgetc (fimg); while (ch != '\n' && ch != '\r' && ch != EOF);
				if (ch == EOF) return 3;
				continue;
			}
			if (ch < '0' || ch > '9') return 4;
			while (ch >= '0' && ch <= '9') {
				dim[i] = dim[i] * 10 + (ch - '0');
				ch = fgetc (fimg);
			}
			if (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') return 5;
			break;
		}
	}
	/* bits por pixel (24) */
	if (dim[2] != 255 /*&& dim[2] != 65535*/) return 6;

	*tam_dados = flen (fimg) - ftell (fimg);
	if (*tam_dados != (ulong)((dim[0] * dim[1] * (dim[2] == 255 ? 3 : 6)))) return 7;

	return 0;
}


ulong flen (FILE *fd)
{
	ulong ret, pos;
	if (fd == NULL) return -1;
	pos = ftell (fd);
	fseek (fd, 0, SEEK_END);
	ret = ftell (fd);
	fseek (fd, pos, SEEK_SET);
	return ret;
}

/* rotaciona os bits para esquerda em 1 posicao */
ulong RotEsq (ulong n)
{
	return (n << 1) | (n >> 31);
}

/* Versão bufferizada de fputc. Consegue bufferizar uma saída por vez.
Para dar flush, chamar com NULL em fo.
*/
void Fputc (int ch, FILE *fo)
{
	static int pt = -1;
	static byte buf[4096];
	static FILE *fo_s = NULL;

	if (pt < 0 && fo != NULL) {
		pt = 0;
		fo_s = fo;
	}
	if (fo_s != fo && fo_s != NULL) {
		if (pt > 0) fwrite (buf, pt, 1, fo_s);
		if (fo != NULL) {
			pt = 0;
			fo_s = fo;
		}
		else {
			pt = -1;
			fo_s = NULL;
			return;
		}
	}
	if (fo == NULL) return;
	buf[pt] = (byte) (ch & 0xff);
	pt++;
	if (pt == 4096) {
		fwrite (buf, pt, 1, fo_s);
		pt = 0;
	}
}
