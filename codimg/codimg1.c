/*
Algoritmo codimg

tamanho minimo txt: 1 byte
tamanho minimo img: 33 bytes

primeiros 32 bytes da �rea de dados da imagem devem guardar tamanho do txt, 1
bit por byte, em notacao big-endian.

�rea de dados (isto �, sem cabe�alho) da imagem deve ter tamanho minimo, em bytes, de 32 + tamanho txt

calcular numero de bits a alterar por byte
da seguinte forma:

completar primeiro os bits menos significativos, at� que seja possivel
codificar todos os bits. Pegar os bits que sobraram no nivel menos
significativo que ainda esta incompleto, e usar para montar blocos de
mesmo tamanho, em que os primeiros bytes ter�o um bit a mais mudado,
em comparacao com os ultimos bytes, que guardarao os bits que sobraram
sem ser utilizados. Utilizar preenchimento POR BLOCOS em formato
big-endian da
esquerda pra direita, **POR NIVEIS** dentro do bloco, e nao por bytes,
para nao dar distorcao grande no ultimo nivel, que poderia ter bytes
sem mudar e outros muito mudados. O numero maximo de blocos sera 1024
bytes para evitar alocacao dinamica de memoria


Outra Explica��o (do c�lculo), mais detalhada:

Calcular o tamanho suficiente e necess�rio dos blocos, limitando a 1024 bytes,
de forma a ocupar sempre os bits menos significativos, e com
espalhamento o mais uniforme poss�vel no n�vel mais significativo
ocupado.
Calcular a quantidade necess�ria e suficiente de blocos.
Calcular o tamanho do �ltimo bloco, que poder� ser menor.
Calcular o n�mero de bits do �ltimo bloco, observando que n�o poder�
passar o n�mero de n�veis previamente estipulados.
Calcular quantos bytes ficaram de fora dos blocos necess�rios. Se
houver, quer dizer que o �ltimo bloco teve tamanho normal. caso contr�rio,
o �ltimo bloco pode ter sido menor.


para calcular o tamanho necess�rio e suficiente do bloco:
distribuir do n�vel menos para o mais significativo. O �ltimo n�vel
provavelmente ficar� incompleto. No caso raro de preencher exatamente
o �ltimo bloco, usar bloco de tamanho 1024 bytes (1 KiB). Se houver
mais bits usados que n�o usados no �ltimo bloco, calcular o tamanho de
bloco que cont�m apenas um bit n�o usado no �ltimo n�vel. Caso haja
mais bits n�o usados, calcular o tamanho do bloco que conter� apenas
um bit usado no �ltimo n�vel. Em qualquer caso, se o bloco ficar com
mais de 1 KiB, reduzir para 1024 bites. Se a redu��o foi feita no caso
de mais bits usados que n�o usados no �ltimo bloco, ent�o todos os
bits do �ltimo n�vel ser�o preenchidos, como se fosse o caso em que
preencheu exatamente o �ltimo n�vel. Aten��o especial deve ser dada
aos arredondamentos, pois alguns devem ser para cima e outros para
baixo, sempre observando o necess�rio para gravar os bits, sem ultrapassar
o que for suficiente para grav�-los.


*/


#include <stdio.h>
#include <stdlib.h>

typedef unsigned long ulong;
typedef unsigned char byte;


void DeCodifica (char *arqTxt, char *arqImg);
void Codifica (char *arqTxt, char *arqImg, char *arqOut);
byte BitEsq (ulong c);
ulong RotEsq (ulong n);
int CalculaTamanhos (ulong tam_txt, ulong tam_dados, int *b, int *n);
int ExtraiCabecalhoImagem (FILE *fimg, ulong *tam_dados);
int ExtraiCabecalhoImagemBMP (FILE *fimg, ulong *tam_dados);
ulong ArredCima (double n);
ulong flen (FILE *fd);


int main (int argc, char *argv[])
{
  if (argc != 4 && argc != 3) {
    fprintf (stderr, "sintaxe:\n"
	     "\tcodificar: %s secreto.txt img.bmp out.bmp\n"
	     "\tdecodificar: %s img.bmp secreto.txt\n", argv[0],
             argv[0]);
    return 1;
  }

  if (argc == 4) {
    Codifica (argv[1], argv[2], argv[3]);
  }
  else if (argc == 3) {
    DeCodifica (argv[2], argv[1]);
  }

  return 0;
}

void DeCodifica (char *arqTxt, char *arqImg)
{
  FILE *ftxt, *fimg;
  ulong tam_txt;
  ulong tam_dados;
  int b, n; /* tamanho bloco e quantidade de bits por bloco */
  byte buffer[1024];
  int i, j;
  int nivel;
  int pt_byte;
  int n_blocos;
  int car_txt; /* caracter do txt */
  int bits_car_txt; /* quantidade de bits disponiveis em car_txt */
  ulong acum_bits_gravados; /* bits do txt */
  ulong acum_bytes_lidos; /* bytes da imagem */


  fimg = fopen (arqImg, "rb");
  if (fimg == NULL) {
    fprintf (stderr, "Erro ao abrir %s\n", arqImg);
    exit (1);
  }

  if (ExtraiCabecalhoImagem (fimg, &tam_dados) != 0) {
    fprintf (stderr, "Erro no cabecalho da imagem\n");
    fclose (fimg);
    exit (1);
  }

  if (tam_dados < 33) {
    fclose (fimg);
    fprintf (stderr, "Imagem e' pequena, nao contem conteudo secreto\n");
    exit (1);
  }

  tam_txt = 0;
  for (i = 0; i < 32; i++) tam_txt = (tam_txt << 1) | (fgetc (fimg) & 1);
  acum_bytes_lidos = 32;
  if (tam_txt > tam_dados - 32 || tam_txt < 1) {
    fclose (fimg);
    fprintf (stderr, "Imagem nao contem conteudo secreto\n");
    exit (1);
  }

  /* calculo do tamanho do bloco e quantos bits por bloco */
  if (CalculaTamanhos (tam_txt, tam_dados, &b, &n) != 0) {
    fclose (fimg);
    fprintf (stderr, "Erro no calculo da distribuicao de bits\n"
                     "Enviar os dados abaixo para o autor do programa:\n\n"
                     "Funcao de decodificacao\n"
                     "Tamanho dados imagem: %lu\n"
                     "Tamanho texto secreto: %lu\n"
                     "Tamanho bloco: %d\n"
                     "Bits por bloco: %d\n",
                     tam_dados, tam_txt, b, n
            );
    exit (1);
  }

  ftxt = fopen (arqTxt, "wb");
  if (ftxt == NULL) {
    fclose (fimg);
    fprintf (stderr, "Erro ao criar %s\n", arqTxt);
    exit (1);
  }

  printf ("Tamanho bloco: %d\n", b);
  printf ("Bits por bloco: %d\n", n);

  /* apertem os cintos, inicio da decodificacao! */

  car_txt = 0;
  bits_car_txt = 0; /* quantidade de bits do txt secreto em car_txt */  
  n_blocos = ArredCima ((8.0 * tam_txt) / n);
  printf ("Quantidade blocos: %d\n", n_blocos);
  for (i = n_blocos - 1; i > 0; i--) { /* loop dos blocos exceto ultimo */
    fread (buffer, b, 1, fimg);
    nivel = 0;
    pt_byte = 0;
    for (j = 0; j < n; j++) {
      car_txt = (car_txt << 1) | ((buffer[pt_byte] >> nivel) & 1);
      if (++pt_byte >= b) {
        pt_byte = 0;
        nivel++;
      }
      if (++bits_car_txt >= 8) {
        fputc (car_txt, ftxt);
        bits_car_txt = 0;
        car_txt = 0;
      }
    } /* end for (j) */
  } /* end for (i) */
  acum_bits_gravados = n * (n_blocos - 1);
  acum_bytes_lidos += b * (n_blocos - 1);

  /* tratar bloco final, que pode ser menor que os outros */
  n = 8 * tam_txt - acum_bits_gravados;
  if ((i = tam_dados - acum_bytes_lidos) < b) b = i;
  if (ArredCima ((double)n / b) >
      ArredCima ((8.0 * tam_txt) / (tam_dados - 32))
     ) {
    fclose (ftxt);
    fclose (fimg);
    fprintf (stderr, "Erro: bloco final acumulou mais bits que o"
                     "permitido (decodificacao)\n"
                     "Tamanho ultimo bloco: %d\n"
                     "Bits restantes: %d\n", b, n
            );
    exit (1);
  }
  printf ("Tamanho ultimo bloco: %d\n"
          "Bits ultimo bloco: %d\n", b, n
         );
  fread (buffer, b, 1, fimg);
  nivel = 0;
  pt_byte = 0;
  for (j = 0; j < n; j++) {
    car_txt = (car_txt << 1) | ((buffer[pt_byte] >> nivel) & 1);
    if (++pt_byte >= b) {
      pt_byte = 0;
      nivel++;
    }
    if (++bits_car_txt >= 8) {
      fputc (car_txt, ftxt);
      bits_car_txt = 0;
      car_txt = 0;
    }
  } /* end for (j) */
  acum_bits_gravados += n;
  /* fim do tratamento do ultimo bloco */

  fclose (ftxt);
  fclose (fimg);

  /* conferir se tudo foi gravado */
  if (acum_bits_gravados != 8 * tam_txt || bits_car_txt != 0) {
    fprintf (stderr, "Erro: quantidade errada de bits gravados, ou\n"
                     "sobra de bits na variavel de decodificacao\n"
                     "Gravados: %lu"
                     "Sobra de bits: %d", acum_bits_gravados, bits_car_txt
            );
    exit (1);
  }


} /* fim decodifica */


void Codifica (char *arqTxt, char *arqImg, char *arqOut)
{

  FILE *ftxt, *fimg, *fout;
  ulong tam_txt;
  ulong tam_dados;
  int b, n; /* tamanho bloco e quantidade de bits por bloco */
  byte buffer[1024];
  int i, j;
  int nivel;
  int pt_byte;
  int n_blocos;
  int car_txt; /* caracter vindo do txt */
  int bits_car_txt; /* quantidade de bits disponiveis em car_txt */
  ulong acum_bits_gravados; /* bits do txt */
  ulong acum_bytes_gravados; /* bytes da imagem (area de dados apenas) */


  ftxt = fopen (arqTxt, "rb");
  if (ftxt == NULL) {
    fprintf (stderr, "Erro ao abrir %s\n", arqTxt);
    exit (1);
  }

  fimg = fopen (arqImg, "rb");
  if (fimg == NULL) {
    fclose (ftxt);
    fprintf (stderr, "Erro ao abrir %s\n", arqImg);
    exit (1);
  }

  tam_txt = flen (ftxt);


  if (tam_txt == 0) {
    fprintf (stderr, "Nao ha o que codificar\n");
    fclose (ftxt);
    fclose (fimg);
    exit (1);
  }

  if (ExtraiCabecalhoImagem (fimg, &tam_dados) != 0) {
    fprintf (stderr, "Erro no cabecalho da imagem\n");
    fclose (ftxt);
    fclose (fimg);
    exit (1);
  }

  if (tam_txt > tam_dados - 32) {
    fprintf (stderr, "Imagem muito pequena para guardar codificacao\n");
    fclose (ftxt);
    fclose (fimg);
    exit (1);
  }

  /* calculo do tamanho do bloco e quantos bits por bloco */
  if (CalculaTamanhos (tam_txt, tam_dados, &b, &n) != 0) {
    fclose (ftxt);
    fclose (fimg);
    fprintf (stderr, "Erro no calculo da distribuicao de bits\n"
                     "Enviar os dados abaixo para o autor do programa:\n\n"
                     "Funcao de codificacao\n"
                     "Tamanho dados imagem: %lu\n"
                     "Tamanho texto secreto: %lu\n"
                     "Tamanho bloco: %d\n"
                     "Bits por bloco: %d\n",
                     tam_dados, tam_txt, b, n
            );
    exit (1);
  }

  printf ("Tamanho bloco: %d\n", b);
  printf ("Bits por bloco: %d\n", n);

  fout = fopen (arqOut, "wb");
  if (fout == NULL) {
    fclose (ftxt);
    fclose (fimg);
    fprintf (stderr, "Erro ao criar %s\n", arqOut);
    exit (1);
  }

  fseek (ftxt, 0, SEEK_SET);
  fseek (fimg, 0, SEEK_SET);
  fread (buffer, 54, 1, fimg);
  fwrite (buffer, 54, 1, fout);

  /* apertem os cintos, inicio das codificacoes! */
  for (i = 0; i < 32; i++) {
    tam_txt = RotEsq (tam_txt);
    fputc ((tam_txt & 1) | (fgetc (fimg) & 0xfe), fout);
  }
  acum_bytes_gravados = 32;

  car_txt = fgetc (ftxt);
  bits_car_txt = 8; /* quantidade de bits do txt secreto em car_txt */  
  n_blocos = ArredCima ((8.0 * tam_txt) / n);
  printf ("Quantidade blocos: %d\n", n_blocos);
  for (i = n_blocos - 1; i > 0; i--) { /* loop dos blocos exceto ultimo */
    fread (buffer, b, 1, fimg);
    nivel = 0;
    pt_byte = 0;
    for (j = 0; j < n; j++) {
      buffer[pt_byte] = (buffer[pt_byte] & (~(1 << nivel))) |
                        (BitEsq (car_txt) << nivel);
      if (++pt_byte >= b) {
        pt_byte = 0;
        nivel++;
      }
      if (--bits_car_txt > 0) {
        car_txt <<= 1;
      }
      else {
        bits_car_txt = 8;
        car_txt = fgetc (ftxt);
      }
    } /* end for (j) */
    fwrite (buffer, b, 1, fout);
  } /* end for (i) */
  acum_bits_gravados = n * (n_blocos - 1);
  acum_bytes_gravados += b * (n_blocos - 1);

  /* tratar bloco final, que pode ser menor que os outros */
  n = (8 * tam_txt) - acum_bits_gravados;
  if ((i = tam_dados - acum_bytes_gravados) < b) b = i;
  if (ArredCima ((double)n / b) >
      ArredCima ((8.0 * tam_txt) / (tam_dados - 32))
     ) {
    fclose (ftxt);
    fclose (fimg);
    fclose (fout);
    fprintf (stderr, "Erro: bloco final acumulou mais bits que o"
                     "permitido\n"
                     "Tamanho ultimo bloco: %d\n"
                     "Bits restantes: %d\n", b, n
            );
    exit (1);
  }
  fread (buffer, b, 1, fimg);
  nivel = 0;
  pt_byte = 0;
  printf ("Tamanho ultimo bloco: %d\n"
          "Bits ultimo bloco: %d\n", b, n
         );
  for (j = 0; j < n; j++) {
    buffer[pt_byte] = (buffer[pt_byte] & (~(1 << nivel))) |
                      (BitEsq (car_txt) << nivel);
    if (++pt_byte >= b) {
      pt_byte = 0;
      nivel++;
    }
    if (--bits_car_txt > 0) {
      car_txt <<= 1;
    }
    else {
      bits_car_txt = 8;
      car_txt = fgetc (ftxt);
    }
  } /* end for (j) */
  fwrite (buffer, b, 1, fout);
  acum_bits_gravados += n;
  acum_bytes_gravados += b;
  /* fim do tratamento do ultimo bloco */

  fclose (ftxt);

  /* gravacao do resto da imagem, sem codificacao do txt (que ja acabou) */
  for (i = (tam_dados - acum_bytes_gravados) / 1024; i > 0; i--) {
    fread (buffer, 1024, 1, fimg);
    fwrite (buffer, 1024, 1, fout);
    acum_bytes_gravados += 1024;
  }
  if ((i = tam_dados - acum_bytes_gravados) > 0) {
    fread (buffer, i, 1, fimg);
    fwrite (buffer, i, 1, fout);
    acum_bytes_gravados += i;
  }

  fclose (fimg);
  fclose (fout);

  /* conferir se tudo foi gravado */
  if (acum_bits_gravados != 8 * tam_txt ||
      acum_bytes_gravados != tam_dados ||
      car_txt != EOF) {
    fprintf (stderr, "Erro: quantidade errada de bits modificados, ou\n"
                     "bytes gravados, ou "
                     "sobra de bytes no arquivo txt secreto\n"
                     "Bits modificados: %lu\n"
                     "Bytes gravados: %lu",
                     acum_bits_gravados, acum_bytes_gravados
            );
    exit (1);
  }
}

/* devolve o bit mais significativo de c, assumindo c como tipo byte */
byte BitEsq (ulong c)
{
  return (byte)((c >> 7) & 1);
}

/* rotaciona os bits para esquerda em 1 posicao */
ulong RotEsq (ulong n)
{
  return (n << 1) | (n >> 31);
}

/* retorna diferente de zero se houver erro
 * *b e' o tamanho do bloco
 * *n e' a quantidade de bits por bloco
 */
int CalculaTamanhos (ulong tam_txt, ulong tam_dados, int *b, int *n)
{

  ulong usados; /* bits usados no ultimo nivel */
  ulong sobra;
  ulong aux;

  tam_dados -= 32; /* desconto da codificacao tamanho arquivo */

  usados = (8 * tam_txt) % tam_dados;
  if (usados == 0) usados = tam_dados;

  /* bits que sobram no ultimo nivel preenchido, sem serem usados */
  sobra = tam_dados - usados;

  /* atencao com parenteses nas divisoes: truncamento esta sendo usado */
  if (sobra == 0) {
    *b = 1024;
    *n = *b * ((8 * tam_txt) / tam_dados);
  }
  else if (sobra >= usados) {
    *b = 1 + (sobra / usados);
    if (*b > 1024) *b = 1024; /* limitar tamanho maximo do bloco */
    *n = 1 + *b * ((8 * tam_txt) / tam_dados);
  }
  else {
    *b = 1 + ArredCima ((double)usados / (double)sobra);
    if (*b > 1024) {
      *b = 1024;
      *n = *b * (1 + ((8 * tam_txt) / tam_dados));
    }
    else {
      *n = (*b - 1) + *b * ((8 * tam_txt) / tam_dados);
    }
  }

  if (*b > 1024 || *b < 1 || *n < 1 || *n > (8 * *b)) return 1;

  /* calcular se os parametros calculados sao suficientes para
   * armazenar todo o texto secreto
   */
  aux = tam_dados % *b;
  if (sobra == 0) aux *= (8 * tam_txt) / tam_dados;
  else aux *= 1 + (8 * tam_txt) / tam_dados;
  aux += *b * *n * (tam_dados / *b);
  if (aux < 8 * tam_txt) return 1;

  return 0;
}


/* Podera aceitar novos formatos no futuro */
int ExtraiCabecalhoImagem (FILE *fimg, ulong *tam_dados)
{
  return ExtraiCabecalhoImagemBMP (fimg, tam_dados);
}

/* retorna diferente de zero se houver erro */
int ExtraiCabecalhoImagemBMP (FILE *fimg, ulong *tam_dados)
{
  ulong aux;

  /* tamanho minimo cabecalho */
  if (flen (fimg) < 54) return 1;

  /* assinatura bmp */
  if (fgetc (fimg) != 'B' || fgetc (fimg) != 'M') return 1;

  /* deslocamento ate dados */
  fseek (fimg, 10, SEEK_SET);  
  fread (&aux, 4, 1, fimg);
  if (aux != 54) return 1;

  /* bits por pixel */
  fseek (fimg, 28, SEEK_SET);  
  aux = 0;
  fread (&aux, 2, 1, fimg);
  if (aux != 24 && aux != 32) return 1;

  *tam_dados = flen (fimg) - 54;

  fseek (fimg, 54, SEEK_SET);

  return 0;
}

ulong ArredCima (double n)
{
  if (n - (int) n != 0) return (int) n + 1;
  return (int) n;
}

ulong flen (FILE *fd)
{
  ulong ret, pos;
  if (fd == NULL) return -1;
  pos = ftell (fd);
  fseek (fd, 0, SEEK_END);
  ret = ftell (fd);
  fseek (fd, pos, SEEK_SET);
  return ret;
}


















