/*

Implementacao ascii85 (base 85)
Usa entrada padrao e saida padrao apenas.
Funciona em modo binario mesmo no Windows (com Mingw32).
Ignora caracteres invalidos.
Para decodificar, opcao -d.
Tamanho de linha maximo 72 na codificacao.

Para compilar:
gcc -Wall -Wextra -pedantic -O2 -s -static -o ascii85.exe ascii85.c

*/

#ifdef __MINGW32__
/* Required header file */
#include <fcntl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void decodificar (void)
{
    int ch;
    int i;
    int cont;
    signed int tab[256];
    unsigned long int val = 0;

#ifdef __MINGW32__
    /* Switch to binary mode */
    _setmode(_fileno(stdout),_O_BINARY);
#endif

    for (i = 0; i < 256; i++) tab[i] = -1;
    for (i = 0; i < 85; i++) tab[(int)(i + '!')] = i;

    cont = 0;
    val = 0;
    while ((ch = fgetc (stdin)) != EOF) {
        if (ch == 'z') {
            if (cont != 0) {
                fprintf (stderr, "z em local invalido.\n");
                exit (1);
            }
            fputc (0, stdout);
            fputc (0, stdout);
            fputc (0, stdout);
            fputc (0, stdout);
            val = 0;
            continue;
        }
        if (ch < 0 || ch >= 256 || tab[ch] < 0) {
            continue; /* ignorar caracteres invalidos, espacos, e quebras de linha */
        }
        val = val * 85 + tab[ch];
        if (++cont == 5) {
            cont = 0;
            fputc ((int)((val >> 24) & 0xff), stdout);
            fputc ((int)((val >> 16) & 0xff), stdout);
            fputc ((int)((val >> 8) & 0xff), stdout);
            fputc ((int)(val & 0xff), stdout);
            val = 0;
        }
    }
    if (cont == 0) return;
    /* gravacao dos bytes finais com pad 'u' (que vale 84) */
    if (cont == 1) {
        fprintf (stderr, "Fim inesperado da entrada, sobrou um byte sozinho.\n");
        exit (1);
    }
    for (i = 5 - cont; i > 0; i--) {
        val = val * 85 + 84;
    }
    fputc ((int)((val >> 24) & 0xff), stdout);
    if (cont > 2) fputc ((int)((val >> 16) & 0xff), stdout);
    if (cont > 3) fputc ((int)((val >> 8) & 0xff), stdout);
    val = 0;
}

void codificar (void)
{
    int ch;
    int cont, cont_colunas;
    int i;
    int resto;
    unsigned long val = 0;
    char buf[6];
    
#ifdef __MINGW32__
    /* Switch to binary mode */
    _setmode(_fileno(stdin),_O_BINARY);
#endif

    cont = 0;
    cont_colunas = 0;
    val = 0;
    while ((ch = fgetc (stdin)) != EOF) {
//printf("Val antes: %lu, e vai somar %d (%c)\n", val, ch, ch);
        val = val * 256 + ch;
//printf("Val depois: %lu\n", val);
        
        cont++;
        if (cont == 4) {
            cont = 0;
            if (val == 0) {
                printf ("z");
                if (++cont_colunas >= 72) {
                    cont_colunas = 0;
                    printf ("\n");
                }
            }
            else {
                buf[5] = '\0';
                for (i = 0; i < 5; i++) {
                    resto = val % 85;
                    val -= resto;
                    val /= 85;
                    buf[4 - i] = (char)('!' + resto);
                }
                if (val != 0) {
                    fprintf (stderr, "Erro, val != zero (%lu)\n", val);
                    exit (1);
                }
                for (i = 0; i < 5; i++) {
                    printf ("%c", buf[i]);
                    if (++cont_colunas >= 72) {
                        cont_colunas = 0;
                        printf ("\n");
                    }
                }
            }
//exit(0);
        }
    }

    if (cont == 0) {
        if (cont_colunas != 0) printf ("\n");
        return;
    }

    for (i = 4 - cont; i > 0; i--) { /* pad de zeros */
        val *= 256;
    }

    buf[5] = '\0';
    for (i = 0; i < 5; i++) {
        resto = val % 85;
        val -= resto;
        val /= 85;
        buf[4 - i] = (char)('!' + resto);
    }
    if (val != 0) {
        fprintf (stderr, "Erro, val != zero (%lu)\n", val);
        exit (1);
    }
    for (i = 0; i <= cont; i++) { /* descarta o pad de zeros */
        printf ("%c", buf[i]);
        if (++cont_colunas >= 72) {
            cont_colunas = 0;
            printf ("\n");
        }
    }
    if (cont_colunas != 0) printf ("\n");
    return;
}

int main (int argc, char *argv[])
{
    int i;

    for (i = 1; i < argc; i++) {
        if (strcmp (argv[i], "-d") == 0) {
            decodificar ();
            break;
        }
    }

    if (i >= argc) codificar ();

    return 0;
}
